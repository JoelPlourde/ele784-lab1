#include <generated/autoconf.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/device.h>
#include <linux/init.h>

#include <linux/string.h>
#include <linux/syscalls.h>
#include <linux/types.h>
#include <linux/stat.h> 
#include <linux/semaphore.h>
#include <linux/spinlock.h>
#include <linux/interrupt.h>

#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/cdev.h>

#define BAUDRATE 115200
#define LOCALBUFSIZE 64
#define DATASIZE 8
#define PARITY 0
#define BUFSIZE 255

#define O_BLOCK 69

#define FCLK 1843200

#define SERIAL_IOC_MAGIC 'x'

#define SERIAL_IOC_SET_BAUDRATE _IO(SERIAL_IOC_MAGIC, 0)
#define SERIAL_IOC_SET_DATASIZE _IO(SERIAL_IOC_MAGIC, 1)
#define SERIAL_IOC_SET_PARITY   _IO(SERIAL_IOC_MAGIC, 2)
#define SERIAL_IOC_GET_BUFSIZE  _IO(SERIAL_IOC_MAGIC, 3)
#define SERIAL_IOC_SET_BUFSIZE  _IO(SERIAL_IOC_MAGIC, 4)

#define SERIAL_IOC_MAXNR 6

#define SERIAL_NUMBER_REGISTERS 8
#define SERIAL_BASE_ADDRESS_PORT_0 0xc030
#define SERIAL_BASE_ADDRESS_PORT_1 0xc020

#define SERIAL_IER_ETBEI   0x01
#define SERIAL_IER_ERBFI   0x02

#define SERIAL_FCR_FIFOEN  0x01
#define SERIAL_FCR_RCVRRE  0x02
#define SERIAL_FCR_RCVRTRL 0x40
#define SERIAL_FCR_RCVRTRM 0x80

#define SERIAL_LCR_WLS0    0x01
#define SERIAL_LCR_WLS1    0x02
#define SERIAL_LCR_STB     0x04
#define SERIAL_LCR_PEN     0x08
#define SERIAL_LCR_EPS     0x10
#define SERIAL_LCR_DLAB    0x80

#define SERIAL_LSR_DR      0x01
#define SERIAL_LSR_OE      0x02
#define SERIAL_LSR_PE      0x04
#define SERIAL_LSR_FE      0x08
#define SERIAL_LSR_THRE    0x20
#define SERIAL_LSR_TEMT    0x40

#define SERIAL_FCR_FIFOEN  0x01
#define SERIAL_FCR_RCVRRE  0x02
#define SERIAL_FCR_RCVRTRL 0x40
#define SERIAL_FCR_RCVRTRM 0x80

#define SERIAL_ISR_0 20
#define SERIAL_ISR_1 21

MODULE_AUTHOR("Joel Plourde");
MODULE_LICENSE("Dual BSD/GPL");

int PiloteVar = 12;
module_param(PiloteVar, int, S_IRUGO);
EXPORT_SYMBOL_GPL(PiloteVar);

typedef struct cBuffer {
	char *buffer;
	int idOut;
	int idIn;
	int count;
	spinlock_t lock;
} cBuffer; 

typedef struct ioctl {
	unsigned int baudrate;
	unsigned int datasize;
	unsigned int parity;
	unsigned int bufsize;
	spinlock_t lock;
} ioctl;

typedef struct sRegister {
	unsigned long RBR;
	unsigned long THR;
	unsigned long DLL;
	unsigned long DLM;
	unsigned long IER;
	unsigned long IIR;
	unsigned long FCR;
	unsigned long LCR;
	unsigned long MCR;
	unsigned long LSR;
	unsigned long MSR;
	unsigned long SCR;
	struct semaphore sem;
} sRegister;

struct Driver {
	struct Device *device0;
	struct Device *device1;
	struct class *cclass;
} Driver;

struct Device {
	dev_t dev;
	int flags;
	int mode;
	int uid;
	int irq;
	bool initialize;
	struct cdev  cdev;
	struct sRegister *sReg;
	struct cBuffer *TxBuf;
	struct cBuffer *RxBuf;
	struct ioctl *var;
	struct semaphore sem;
} Device;

struct Driver *driver;

// Queue
wait_queue_head_t queue;

// Initialize and Exit the Driver.
static int pilote_init (void);
static void pilote_exit (void);

// Driver operations.
static ssize_t module_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos);
static ssize_t module_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos);
static long    module_ioctl(struct file *filp, unsigned int cmd, unsigned long arg);
static int     module_open(struct inode *inode, struct file *filp);
static int     module_release(struct inode *inode, struct file *filp);

static ssize_t device_read(struct Device *device, struct file *filp, char __user *buf, size_t count, loff_t *f_pos);
static ssize_t device_write(struct Device *device, struct file *filp, const char __user *buf, size_t count, loff_t *f_pos);
static long    device_ioctl(struct Device *device, struct file *filp,unsigned int cmd, unsigned long arg);
static int     device_release(struct Device *device);
static int     device_open(struct file *filp, uint64_t minor, struct Device *device, int irq, unsigned long base_addr, char* name);
static int     device_exit(struct Driver *driver, struct Device *device);

static struct file_operations fops = {
		.owner 	 = THIS_MODULE,
		.write	 = module_write,
		.read	 = module_read,
		.open	 = module_open,
		.release = module_release,
		.unlocked_ioctl = module_ioctl,
};

// Circular buffer operations.
static void cBuffer_init(cBuffer *cb, size_t capacity);
static void cBuffer_free(cBuffer *cb);
static char cBuffer_read(cBuffer *cb, struct ioctl *var);
static int  cBuffer_write(cBuffer *cb, struct ioctl *var, char data);

// Initialization
static void ioctl_init(ioctl *variable);
static void register_init(sRegister *regi, unsigned long base_addr);

// Operational function.
static int check_permissions(struct file *filp, struct Device *device, int uid);

// Interruption function.
static irqreturn_t irq_handler (int irq, void *dev_id);

static irqreturn_t irq_handler(int irq, void *dev_id)
{
	// Retrieve the device related to this IRQ
	struct Device *device = (struct Device*) dev_id;

	if(device != NULL)
	{
		if(device->irq == SERIAL_ISR_0 || device->irq == SERIAL_ISR_1)
		{
			// Disable IRQ
			outb((unsigned char) 0x00, device->sReg->IER);

			if((inb(device->sReg->LSR) & SERIAL_LSR_DR) == SERIAL_LSR_DR)
			{
				// Read the received data.
				unsigned char data = inb(device->sReg->RBR);

				if((data != '\0') && (data != NULL) && (data != '\xe0') && (data != '\x80') && (data != '\xc0') && (data != '\xff'))
				{
					printk(KERN_WARNING"RX : %c\n", data);
					cBuffer_write(device->RxBuf, device->var, data);
				}

				// Enable IRQ
				outb((unsigned char) SERIAL_IER_ETBEI | SERIAL_IER_ERBFI, device->sReg->IER);

				return IRQ_HANDLED;
			}
			// Transmission handler.
			else if((inb(device->sReg->LSR) & SERIAL_LSR_THRE) == SERIAL_LSR_THRE)
			{
				// Extract a data from the TxBuf
				char data = cBuffer_read(device->TxBuf, device->var);

				if(data != NULL)
				{
					printk(KERN_WARNING"writing in device : %d\n", device->irq);

					printk(KERN_WARNING"TX : %c\n", data);
					outb((unsigned char) data, device->sReg->THR);
				}

				// Enable IRQ
				outb((unsigned char) SERIAL_IER_ETBEI | SERIAL_IER_ERBFI, device->sReg->IER);

				return IRQ_HANDLED;
			}
			else
			{
				// Enable IRQ
				outb((unsigned char) SERIAL_IER_ETBEI | SERIAL_IER_ERBFI, device->sReg->IER);

				return IRQ_NONE;
			}

			// Enable IRQ
			outb((unsigned char) SERIAL_IER_ETBEI | SERIAL_IER_ERBFI, device->sReg->IER);

			return IRQ_HANDLED;
		}
	}

	return IRQ_NONE;
}


static long module_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	return device_ioctl(((uint64_t) filp->private_data == 0) ? driver->device0 : driver->device1, filp, cmd, arg);
}

static ssize_t module_read(struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
	printk(KERN_WARNING"Private Data: %d\n", (uint64_t) filp->private_data);

	return device_read(((uint64_t) filp->private_data == 0) ? driver->device0 : driver->device1, filp, buf, count, f_pos);
}

static ssize_t module_write(struct file *filp, const char __user *buf, size_t count, loff_t *f_pos)
{
	return device_write(((uint64_t) filp->private_data == 0) ? driver->device0 : driver->device1, filp, buf, count, f_pos);
}

static int module_open(struct inode *inode, struct file *filp) 
{
	int ret = 0;
	uint64_t minor = (uint64_t) iminor(inode);

	printk(KERN_WARNING"Driver opened : %d\n", (uint32_t) minor);

	if(minor == 0)
	{
		printk(KERN_WARNING"opening 21!\n");

		disable_irq(SERIAL_ISR_0);
		ret = device_open(filp, minor, driver->device0, SERIAL_ISR_0, SERIAL_BASE_ADDRESS_PORT_0, "serial_port_0");
		enable_irq(SERIAL_ISR_0);
	}
	else
	{
		printk(KERN_WARNING"opening 22!\n");

		disable_irq(SERIAL_ISR_1);
		ret = device_open(filp, minor, driver->device1, SERIAL_ISR_1, SERIAL_BASE_ADDRESS_PORT_1, "serial_port_1");
		enable_irq(SERIAL_ISR_1);
	}

	printk(KERN_WARNING"Driver opened finished : %d\n", (uint32_t) minor);

	return ret;
}

static int module_release(struct inode *inode, struct file *filp) 
{
	return device_release(((uint64_t) iminor(inode) == 0) ? driver->device0 : driver->device1);
}

static int __init pilote_init (void)
{
	driver = kmalloc(sizeof(Driver), GFP_KERNEL);

	driver->device0 = kmalloc(sizeof(Device), GFP_KERNEL);
	driver->device1 = kmalloc(sizeof(Device), GFP_KERNEL);

	driver->device0->initialize = false;
	driver->device1->initialize = false;

	alloc_chrdev_region(&(driver->device0->dev), 0, 1, "Serial 0");
	alloc_chrdev_region(&(driver->device1->dev), 1, 1, "Serial 1");

	driver->cclass = class_create(THIS_MODULE, "DRIVER");
	device_create(driver->cclass, NULL, driver->device0->dev, NULL, "etsele_cdev_0");
	device_create(driver->cclass, NULL, driver->device1->dev, NULL, "etsele_cdev_1");

	cdev_init(&(driver->device0->cdev), &fops);
	cdev_init(&(driver->device1->cdev), &fops);

	driver->device0->cdev.owner = THIS_MODULE;
	driver->device1->cdev.owner = THIS_MODULE;

	driver->device0->uid = -1;
	driver->device1->uid = -1;

	init_waitqueue_head(&queue);

	cdev_add(&(driver->device0->cdev), driver->device0->dev, 1);
	cdev_add(&(driver->device1->cdev), driver->device1->dev, 1);

	printk(KERN_WARNING"Driver : %u Hello, world !\n", PiloteVar);

	return 0;
}

static void __exit pilote_exit (void)
{
	disable_irq(SERIAL_ISR_0);
	disable_irq(SERIAL_ISR_1);

	if(driver->device0->initialize)
	{
		device_exit(driver, driver->device0);
	}

	if(driver->device1->initialize)
	{
		device_exit(driver, driver->device1);
	}

	cdev_del(&(driver->device0->cdev));
	cdev_del(&(driver->device1->cdev));

	device_destroy(driver->cclass, driver->device0->dev);
	device_destroy(driver->cclass, driver->device1->dev);
	class_destroy(driver->cclass);

	unregister_chrdev_region(driver->device0->dev, 1);
	unregister_chrdev_region(driver->device1->dev, 1);

	kfree(driver->device0);
	kfree(driver->device1);

	kfree(driver);

	printk(KERN_ALERT"Driver : Goodbye, cruel world\n");
}

static void cBuffer_init(cBuffer *cb, size_t capacity)
{
	cb->buffer = kmalloc(sizeof(char) * capacity, GFP_KERNEL);

	spin_lock_init(&(cb->lock));

	// Get the semaphore
	spin_lock(&(cb->lock));

	cb->idOut = 0;
	cb->idIn = 0;
	cb->count = 0;

	spin_unlock(&(cb->lock));
}

static int  cBuffer_write(cBuffer *cb, struct ioctl *var, char data)
{
	unsigned int size;

	if (cb == NULL || cb->buffer == NULL || var == NULL)
	{
		printk(KERN_ALERT"WRITE : The Circular Buffer is null.\n");
		return -1;
	}
	else
	{
		if(data == NULL)
		{
			printk(KERN_ALERT"WRITE : Data received is null.\n");
			return NULL;
		}

		//spin_lock(&(var->lock));
		size = var->bufsize;
		//spin_unlock(&(var->lock));

		//spin_lock(&(cb->lock));

		cb->buffer[cb->idOut] = data;

		cb->idOut = (cb->idOut + 1) % size;
		cb->count = cb->count + 1;

		printk(KERN_ALERT"WRITE : Current count : %d\n", cb->count);

		//spin_unlock(&(cb->lock));

		wake_up_interruptible(&queue);

		return 1;
	}
}

static char cBuffer_read(cBuffer *cb, struct ioctl *var)
{
	char data = 0;
	unsigned int size;

	if (cb == NULL || var == NULL || cb->buffer == NULL)
	{
		printk(KERN_ALERT"READ : The Circular Buffer is null.\n");
		return -1;
	}
	else
	{
		if(cb->count > 0)
		{
			//spin_lock(&(var->lock));
			size = var->bufsize;
			//spin_unlock(&(var->lock));

			//spin_lock(&(cb->lock));

			data = cb->buffer[cb->idIn];

			cb->idIn = (cb->idIn + 1) % size;
			cb->count = cb->count - 1;

			//spin_unlock(&(cb->lock));

			wake_up_interruptible(&queue);

			return data;
		}
		else
		{
			return '\0';
		}


		return data;
	}
}

static void cBuffer_free(cBuffer *cb)
{
	kfree(cb->buffer);
}

static void ioctl_init(ioctl *variable)
{
	spin_lock_init(&(variable->lock));

	// Get the semaphore
	spin_lock(&(variable->lock));

	// Initialize the serial parameters.
	variable->baudrate = BAUDRATE;
	variable->datasize = DATASIZE;
	variable->parity = PARITY;
	variable->bufsize = BUFSIZE;

	// Release the semaphore
	spin_unlock(&(variable->lock));
}

static void register_init(sRegister *regi, unsigned long base_addr)
{
	sema_init(&(regi->sem), 1);

	// Get the semaphore
	down(&(regi->sem));

	regi->RBR = base_addr;
	regi->THR = base_addr;
	regi->DLL = base_addr;
	regi->DLM = base_addr + 1;
	regi->IER = base_addr + 1;
	regi->IIR = base_addr + 2;
	regi->FCR = base_addr + 2;
	regi->LCR = base_addr + 3;
	regi->MCR = base_addr + 4;
	regi->LSR = base_addr + 5;
	regi->MSR = base_addr + 6;
	regi->SCR = base_addr + 7;

	// Initialize each registers to their default state.
	outb((unsigned char) 0x00, base_addr);
	outb((unsigned char) 0x00, base_addr + 1);
	outb((unsigned char) 0x00, base_addr + 2);
	outb((unsigned char) 0x00, base_addr + 3);
	outb((unsigned char) 0x00, base_addr + 4);
	outb((unsigned char) 0x00, base_addr + 5);
	outb((unsigned char) 0x00, base_addr + 6);
	outb((unsigned char) 0x00, base_addr + 7);

	// Initialize LCR register (DLAB = 1)
	outb((unsigned char) SERIAL_LCR_DLAB | SERIAL_LCR_WLS0 | SERIAL_LCR_WLS1, regi->LCR);

	// Initialize FCR register
	outb((unsigned char) SERIAL_FCR_FIFOEN, regi->FCR);

	// Initialize DL register at a baudrate of 115200
	outb((unsigned char) 0x01, regi->DLL);

	// Reset the (DLAB to 0)
	outb((unsigned char) (inb(regi->LCR) & ~SERIAL_LCR_DLAB), regi->LCR);

	// Enable the Reception and Transmission interruption
	outb((unsigned char) SERIAL_IER_ETBEI | SERIAL_IER_ERBFI, regi->IER);

	// Release the sema
	up(&(regi->sem));
}

static int check_permissions(struct file *filp, struct Device *device, int uid)
{
	down(&(device->sem));

	// Check if the current Device uid is the same as the one received in parameter.
	if(device->uid == uid)
	{
		device->mode =((filp->f_flags & O_NONBLOCK) == O_NONBLOCK) ? O_NONBLOCK : O_BLOCK;

		if((filp->f_flags & O_ACCMODE) == O_WRONLY)
		{
			if(device->flags == -1)
			{
				device->flags = O_WRONLY;
				printk(KERN_WARNING"Pilote opened in WRONLY !\n");
			}
			else if((device->flags & O_RDONLY) == O_RDONLY)
			{
				device->flags |= O_WRONLY;
				printk(KERN_WARNING"Pilote opened in WRONLY with RDONLY!\n");
			}
			else
			{
				printk(KERN_WARNING"REFUSED !\n");
				up(&(device->sem));
				return -ENOTTY;
			}
		}
		else if((filp->f_flags & O_ACCMODE) == O_RDONLY)
		{
			if(device->flags == -1)
			{
				device->flags = O_RDONLY;
				printk(KERN_WARNING"Pilote opened in RDONLY !\n");
			}
			else if((device->flags & O_WRONLY) == O_WRONLY)
			{
				device->flags |= O_RDONLY;
				printk(KERN_WARNING"Pilote opened in RDONLY with WRONLY!\n");
			}

			else
			{
				printk(KERN_WARNING"REFUSED !\n");
				up(&(device->sem));
				return -ENOTTY;
			}
		}
		else if((filp->f_flags & O_ACCMODE) == O_RDWR)
		{
			if(device->flags == -1)
			{
				device->flags = O_RDWR;
				printk(KERN_WARNING"Pilote opened in RDWR !\n");
			}
			else
			{
				printk(KERN_WARNING"REFUSED in RDWR!\n");
				up(&(device->sem));
				return -ENOTTY;
			}
		}
	}
	else
	{
		printk(KERN_WARNING"Driver is currently in used by: %d !\n", device->uid);
		up(&(device->sem));
		return -EPERM;
	}

	up(&(device->sem));

	return 0;
}

static ssize_t device_write(struct Device *device, struct file *filp, const char __user *buf, size_t count, loff_t *f_pos)
{
	int i = 0;
	int tmp = 0;
	int err = 0;
	char BufW[LOCALBUFSIZE];
	int counter = 0;
	int currentcount = 0;

	copy_from_user(BufW, (const void*) buf, count);

	printk(KERN_WARNING"copied from user.\n");

	if((device->mode & O_NONBLOCK) == O_NONBLOCK)
	{
		printk(KERN_WARNING"Pilote WRITE in NONBLOCK mode\n");

		tmp = device->var->bufsize;

		// If there's space in the cBuffer for all the count.
		if(device->TxBuf->count + count <= tmp)
		{
			for(i = 0; i < count; i++)
			{
				err = cBuffer_write(device->TxBuf, device->var, BufW[i]);

				if(err == 0)
					printk(KERN_WARNING"Unable to write data in the buffer.\n");
				else
				{
					counter++;
				}
			}
		}

		printk(KERN_WARNING"Pilote WRITE (amount of character written): %u\n", counter);

		return counter;
	}
	else
	{
		printk(KERN_WARNING"Pilote WRITE in BLOCK mode\n");

		tmp = device->var->bufsize;

		while(device->TxBuf->count + count > tmp)
		{
			wait_event_interruptible_timeout(queue, device->TxBuf->count + count <= tmp, 1000);
		}

		for(i = 0; i < count; i++)
		{
			printk(KERN_WARNING"Data : %c\n", BufW[i]);

			err = cBuffer_write(device->TxBuf, device->var, BufW[i]);

			if(err == 0)
				printk(KERN_WARNING"Unable to write data in the buffer.\n");
			else
				counter++;
		}
	}

	return counter;
}

static ssize_t device_read(struct Device *device, struct file *filp, char __user *buf, size_t count, loff_t *f_pos)
{
	char BufR[LOCALBUFSIZE];
	int counter = 0;
	int currentcount = 0;

	int i = 0;

	if((device->mode & O_NONBLOCK) == O_NONBLOCK)
	{
		for(i = 0; i < count; i++)
		{
			printk(KERN_WARNING"i : %d\n", i);

			if(device->RxBuf->count > 0)
			{
				char data = cBuffer_read(device->RxBuf, device->var);

				if(data != 0)
				{
					BufR[i] = data;
					counter++;
				}
				else
				{
					printk(KERN_WARNING"Error reading a character in the buffer !.\n");
				}
			}
			else
			{
				break;
			}
		}
	}
	else
	{
		printk(KERN_WARNING"Pilote READ in BLOCK mode\n");

		while(device->RxBuf->count < count)
		{
			wait_event_interruptible_timeout(queue, device->RxBuf->count >= count, 1000);
		}

		for(i = 0; i < count; i++)
		{
			char data = cBuffer_read(device->RxBuf, device->var);

			if(data != '\0' && data != NULL)
			{
				BufR[i] = data;
				counter++;
			}
			else
			{
				printk(KERN_WARNING"Error reading a character in the buffer !.\n");
			}
		}
	}

	copy_to_user(buf, BufR, counter);

	if(counter == 0)
	{
		return -EAGAIN;
	}

	return counter;
}

static int device_exit(struct Driver *driver, struct Device *device)
{
	device->uid = -1;												// Reset the flags
	device->initialize = false;

	free_irq(device->irq, (void*) device);							// Free the IRQ

	cBuffer_free(device->TxBuf);									// Free the cBuffer
	cBuffer_free(device->RxBuf);
    kfree(device->TxBuf);
    kfree(device->RxBuf);
	kfree(device->var);												// Free the Variables.
	kfree(device->sReg);											// Free the registers.

	return 0;
}

static int device_open(struct file *filp, uint64_t minor, struct Device *device, int irq, unsigned long base_addr, char* name)
{
	int ret = 0;
	int uid = current_uid().val;									// Get the user id.

	if(device->initialize == true)
	{
		// This device has already been initialized, simply check for permissions.
		ret = check_permissions(filp, device, uid);

		filp->private_data = (void*) minor;

		// Enable back the interruption has they were disabled by the release.
		outb((unsigned char) SERIAL_IER_ETBEI | SERIAL_IER_ERBFI, device->sReg->IER);

		return ret;
	}
	else
	{
		device->irq = irq;
		device->uid = uid;

		sema_init(&(device->sem), 1);									// Initialize the semaphore.

		printk(KERN_WARNING"Requesting region.\n");

		request_region(base_addr, SERIAL_NUMBER_REGISTERS, name);		// Request the memory map for the Port.

		device->sReg = kmalloc(sizeof(sRegister), GFP_KERNEL);

		device->flags = -1;												// Reset the flags and mode to their default values
		device->mode = -1;

		device->var = kmalloc(sizeof(ioctl), GFP_KERNEL);

		ioctl_init(device->var);										// Init the IOCTL

		device->TxBuf = kmalloc(sizeof(cBuffer), GFP_KERNEL);			// Get memory allocation for the Buffers.
		device->RxBuf = kmalloc(sizeof(cBuffer), GFP_KERNEL);

		cBuffer_init(device->TxBuf, device->var->bufsize);				// Initialise the circular buffer.
		cBuffer_init(device->RxBuf, device->var->bufsize);

		filp->private_data = (void*) minor;								// Store the Minor id inside the private_data.

		device->initialize = true;										// Set this device to be initialized.

		ret = check_permissions(filp, device, uid);						// Finally, check if the user has opened the driver correctly, otherwise, quit.

		if(ret >= 0)
		{
			request_irq(irq, irq_handler, IRQF_SHARED, name, (void*) device);	// Request an IRQ for this serial port
			register_init(device->sReg, base_addr);						    // Init the register to their default state.
		}
	}

	return ret;
}

static int device_release(struct Device *device)
{
	down(&(device->sem));

	outb((unsigned char) 0x00, device->sReg->IER);

	device->flags = -1;
	device->mode = -1;

	up(&(device->sem));

	return 0;
}

static long device_ioctl(struct Device *device, struct file *filp, unsigned int cmd, unsigned long arg)
{
	int err = 0;
	unsigned long tmp = 0;
	long retval = 0;

	if (_IOC_TYPE(cmd) != SERIAL_IOC_MAGIC) return -ENOTTY;
	if (_IOC_NR(cmd) > SERIAL_IOC_MAXNR) return -ENOTTY;

	if (_IOC_DIR(cmd) & _IOC_READ)
		err = !access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd));
	else if (_IOC_DIR(cmd) & _IOC_WRITE)
		err = !access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd));
	if (err) return -EFAULT;

	switch(cmd)
	{
		case SERIAL_IOC_SET_BAUDRATE:
			copy_from_user((void*) arg, (const void*) tmp, 1);

			printk(KERN_WARNING"Cmd1 : %d\n", SERIAL_IOC_SET_BAUDRATE);

			if(tmp >= 50 && tmp <= 115200)
			{
				uint8_t dl = FCLK / (16 * tmp);

				down(&(device->sReg->sem));
				// Set the DLAB to 1
				outb((unsigned char) (inb(device->sReg->LCR) | SERIAL_LCR_DLAB), device->sReg->LCR);

				// Set the DLL and the DLM
				outb((unsigned char) dl & 0xFF, device->sReg->DLL);
				outb((unsigned char) dl >> 8, device->sReg->DLM);

				// Reset the DLAB to 0
				outb((unsigned char) (inb(device->sReg->LCR) & ~SERIAL_LCR_DLAB), device->sReg->DLL);
				up(&(device->sReg->sem));

				// Record the new value of the baudrate
				spin_lock(&(device->var->lock));
				device->var->baudrate = (unsigned int)tmp;
				spin_unlock(&(device->var->lock));
			}
			else
				return -ENOTTY;
			break;
		case SERIAL_IOC_SET_DATASIZE:
			copy_from_user((void*) arg, (const void *)tmp, 1);

			printk(KERN_WARNING"Cmd2 : %d\n", SERIAL_IOC_SET_DATASIZE);

			if(tmp >= 5 && tmp <= 8)
			{
				spin_lock(&(device->var->lock));
				device->var->datasize = tmp;
				spin_unlock(&(device->var->lock));
			}
			else
				return -ENOTTY;
			break;
		case SERIAL_IOC_SET_PARITY:
			copy_from_user((void*) arg, (const void *)tmp, 1);

			printk(KERN_WARNING"Cmd3 : %d\n", SERIAL_IOC_SET_PARITY);

			if(tmp == 0 || tmp == 1 || tmp == 2)
			{
				down(&(device->sReg->sem));
				// Parity disable
				if(tmp == 0)
				{
					// Set the PEN bit of the LCR register back to 0.
					outb((unsigned char) (inb(device->sReg->LCR) & ~SERIAL_LCR_PEN), device->sReg->LCR);
				}
				else if(tmp == 1)
				{
					// Set the EPS bit of the LCR register to 0.
					outb((unsigned char) (inb(device->sReg->LCR) & ~SERIAL_LCR_EPS), device->sReg->LCR);
				}
				else
				{
					// Set the EPS bit of the LCR register to 1.
					outb((unsigned char) (inb(device->sReg->LCR) & SERIAL_LCR_EPS), device->sReg->LCR);
				}
				up(&(device->sReg->sem));
			}
			else
				return -ENOTTY;
			break;
		case SERIAL_IOC_GET_BUFSIZE:
			printk(KERN_WARNING"Cmd4 : %d\n", SERIAL_IOC_GET_BUFSIZE);

			spin_lock(&(device->var->lock));
			tmp = device->var->bufsize;
			spin_unlock(&(device->var->lock));
			return tmp;
			break;
		case SERIAL_IOC_SET_BUFSIZE:
			printk(KERN_WARNING"Cmd5 : %d\n", SERIAL_IOC_SET_BUFSIZE);

			if (!capable(CAP_SYS_ADMIN))
				return -EACCES;

			copy_from_user((void*) arg, (const void *)tmp, 1);

			int TxBufSize;
			int RxBufSize;

			spin_lock(&(device->TxBuf->lock));
			TxBufSize = device->TxBuf->count;
			spin_unlock(&(device->TxBuf->lock));

			spin_lock(&(device->RxBuf->lock));
			RxBufSize = device->RxBuf->count;
			spin_unlock(&(device->RxBuf->lock));

			if(TxBufSize <= tmp && RxBufSize <= tmp)
			{
				spin_lock(&(device->var->lock));
				device->var->bufsize = tmp;
				spin_unlock(&(device->var->lock));

				spin_lock(&(device->TxBuf->lock));
				krealloc(device->TxBuf->buffer, tmp, GFP_KERNEL);
				spin_unlock(&(device->TxBuf->lock));

				spin_lock(&(device->RxBuf->lock));
				krealloc(device->RxBuf->buffer, tmp, GFP_KERNEL);
				spin_unlock(&(device->RxBuf->lock));
			}
			else
			{
				printk(KERN_WARNING"ERROR The size of the buffer currently exceeds your requested size !.\n");
				return -ENOTTY;
			}
			break;
		default:
			return -ENOTTY;
			break;
	}

	return retval;
}

module_init(pilote_init);
module_exit(pilote_exit);
