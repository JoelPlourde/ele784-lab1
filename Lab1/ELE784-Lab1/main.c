#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define SERIAL_IOC_SET_BAUDRATE 30720
#define SERIAL_IOC_SET_DATASIZE 30721
#define SERIAL_IOC_SET_PARITY   30722
#define SERIAL_IOC_GET_BUFSIZE  30723
#define SERIAL_IOC_SET_BUFSIZE  30724

int test_opening_sequence();
int test_non_block_mode();

int SelectMode();
int SelectType();
char* SelectDevice();
int OpenDevice();
int CloseDevice(int fd);
void Clear_buffer();
int ReadDeviceMenu();
int WriteDeviceMenu();
int ReadDevice(int filehandler, int nb_char);
int WriteDevice(int filehandler, char* string, int length);

int fds[2] = {-1, -1};
int index = 0;

int main(void)
{
	(test_opening_and_closing() == 0) ? printf("TEST OPENING/CLOSING:  pass\n") : printf("TEST OPENING/CLOSING:  fail\n");
	(test_opening_sequence() == 0) ? printf("TEST OPENING SEQUENCE: pass\n") : printf("TEST OPENING SEQUENCE: fail\n");
	(test_non_block_mode() == 0) ? printf("TEST NON BLOCK MODE:   pass\n") : printf("TEST NON BLOCK MODE:   fail\n");
	(test_block_mode() == 0) ? printf("TEST BLOCK MODE:       pass\n") : printf("TEST BLOCK MODE:       fail\n");
	//(test_ioctl() == 0) ? printf("TEST IOCTL:            pass\n") : printf("TEST IOCTL:            fail\n");



	/*
	int err = 0;

	while(err == 0)
	{
		err = MainMenu();

		if(err < -1)
		{
			printf("Erreur !");
			break;
		}
	}

	int i = 0;
	for(i = 0; i < index; i++)
	{
		if(fds[i] > 0)
		{
			CloseDevice(fds[i]);
		}
	}

	printf("Done !");
	*/

	return 0;
}

int MainMenu()
{
	int cmd;
	int file = -1;

	printf("\n-------------------------------\n");
	printf("Select an action : \n");
	printf("	Open Device:  (1)\n");
	printf("	Read Device:  (2)\n");
	printf("	Write Device: (3)\n");
	printf("	Quit :        (q)\n");
	printf("-------------------------------\n");

	//Clear_buffer();

	cmd = getchar();

	Clear_buffer();

	switch(cmd)
	{
		case '1':
			file = OpenDevice();

			if(file < -1)
			{
				printf("Erreur d'ouverture !");
				return -1;
			}
			else
			{
				printf("Device opened successfully ! : %d\n", file);

				fds[index] = file;
				index++;
			}
			break;
		case '2':
			ReadDeviceMenu();
			break;
		case '3':
			WriteDeviceMenu();
			break;
		case 'q':
			printf("Quit !");
			return -1;
			break;
		default:
			printf("Quit !");
			return -1;
			break;
	}

	return 0;
}

int WriteDeviceMenu()
{
	char device;
	unsigned int len;

	printf("Select which Device to write to : \n");
	printf("	Device 1:  (1)\n");
	printf("	Device 2:  (2)\n");

	device = getchar();
	Clear_buffer();

	printf("Enter the number of character to be written : \n");
	scanf("%d", &len);
	Clear_buffer();

	if( len <= 0)
	{
		return -1;
	}

	char buffer[256];
	int read;
	int nb_char_written = 0;

	printf("Enter the message : \n");
	scanf("%s", &buffer);
	Clear_buffer();

	printf("Message: %s\n", buffer);

	printf("length: %d\n", len);

	printf("Device 1 : %d\n", fds[0]);
	printf("Device 2 : %d\n", fds[1]);

	nb_char_written = write(fds[0], &buffer, len);

	printf("\nNumber of character written : %d\n", nb_char_written);

	if(nb_char_written < -1)
	{
		return -1;
	}

	return 0;
}

int ReadDeviceMenu()
{
	char device;

	printf("Select which Device to read from : \n");
	printf("	Device 1:  (1)\n");
	printf("	Device 2:  (2)\n");

	device = getchar();
	Clear_buffer();

	int nb_char = 0;
	int err = 0;
	printf("Enter the number of character to be read : \n");
	scanf("%d", &nb_char);

	switch(device)
	{
		case '1':
			err = ReadDevice(fds[0], nb_char);

			printf("Retour: %d\n", err);
			break;
		case '2':
			err = ReadDevice(fds[1], nb_char);
			printf("Retour: %d\n", err);
			break;
		default:
			return -1;
			break;
	}
	return 0;
}

int ReadDevice(int filehandler, int nb_char)
{
	char *buffer;

	buffer = malloc(sizeof(char)*nb_char);

	int timeout = 0;
	int count = 0;
	int nb_char_lu = 0;

	while(timeout < 10 && count < nb_char)
	{
		nb_char_lu = read(filehandler, buffer, nb_char);

		printf("Nb of character read: %d\n", nb_char_lu);

		int i = 0;

		for(i = 0; i < nb_char_lu; i++)
		{
			printf("character recu: %c\n", buffer[i]);
		}

		if(nb_char_lu >= 0)
		{
			count += nb_char_lu;

			printf("Count: %d\n", count);
		}

		sleep(1);

		timeout++;
	}
	if(timeout >= 10)
	{
		return -1;
	}

	return 0;
}

int OpenDevice()
{
	int mode = 0;
	int type = 0;
	char* device = "";

	mode = SelectMode();

	if(mode == -1) return -1;

	device = SelectDevice();

	if(device == "EMPTY") return -1;

	type = SelectType();

	if(type == -1) return -1;

	int err = 0;

	err = open(device, mode | type);

	if(device == "/dev/etsele_cdev_0") fds[0] = err;
	if(device == "/dev/etsele_cdev_1") fds[1] = err;

	return err;
}

int CloseDevice(int fd)
{
	close(fd);

	return 0;
}

char* SelectDevice()
{
	int cmd;

	printf("Which device would you like to open : \n");
	printf("	Device 1: (1)\n");
	printf("	Device 2: (2)\n");

	cmd = getchar();

	Clear_buffer();

	switch(cmd)
	{
		case '1':
			return "/dev/etsele_cdev_0";
			break;
		case '2':
			return "/dev/etsele_cdev_1";
			break;
		case 'q':
			break;
		default:
			printf("Unknown command.\n");
			break;
	}

	return "EMPTY";
}

int SelectMode()
{
	int mode;

	printf("Select the mode : \n");
	printf("	READ ONLY : (1)\n");
	printf("	WRITE ONLY : (2)\n");
	printf("	READ/WRITE : (3)\n");

	mode = getchar();
	Clear_buffer();

	switch(mode)
	{
		case '1':
			return O_RDONLY;
			break;
		case '2':
			return O_WRONLY;
			break;
		case '3':
			return O_RDWR;
			break;
		default:
			printf("Unknown command.");
			break;
	}

	return -1;
}

int SelectType()
{
	int type;

	printf("Select the mode : \n");
	printf("	NON BLOCK MODE : (1)\n");
	printf("	BLOCK MODE : (2)\n");

	type = getchar();
	Clear_buffer();

	switch(type)
	{
		case '1':
			return O_NONBLOCK;
			break;
		case '2':
			return 0;
			break;
		default:
			printf("Unknown command.");
			break;
	}

	return -1;
}

/* 	This test consists in opening and closing the same driver multiple time */
int test_opening_and_closing()
{
	int fd;

	fd = open("/dev/etsele_cdev_0", O_RDONLY | O_NONBLOCK);

	if(fd < 0) return -1;
	close(fd);

	fd = open("/dev/etsele_cdev_0", O_WRONLY | O_NONBLOCK);

	if(fd < 0) return -1;
	close(fd);

	fd = open("/dev/etsele_cdev_0", O_RDWR | O_NONBLOCK);

	if(fd < 0) return -1;
	close(fd);

	return 0;
}

// Taken from : https://stackoverflow.com/questions/7898215/how-to-clear-input-buffer-in-c
void Clear_buffer()
{
	int c;
	while ((c = getchar()) != '\n' && c != EOF) { }
}

/* This test opens the same driver in readonly, then writeonly and tries to open it
 * it in RDWR and fails. */
int test_opening_sequence()
{
	int file_handler_1;
	int file_handler_2;
	int file_handler_3;

	file_handler_1 = open("/dev/etsele_cdev_0", O_RDONLY | O_NONBLOCK);
	file_handler_2 = open("/dev/etsele_cdev_0", O_WRONLY | O_NONBLOCK);

	if(file_handler_1 < 0 || file_handler_2 < 0)
	{
		printf("Erreur d'ouverture : %d, %d\n", file_handler_1, file_handler_2);
		return -1;
	}

	file_handler_3 = open("/dev/etsele_cdev_0", O_RDWR | O_NONBLOCK);

	if(file_handler_3 == 0)
	{
		printf("Erreur d'ouverture : %d, %d\n", file_handler_3);
		return -1;
	}

	close(file_handler_1);
	close(file_handler_2);
	close(file_handler_3);

	return 0;
}

/* With the serial port connected with its each, try to write a command and read the same command
 * in block mode.*/
int test_block_mode()
{
	int file_handler_1;
	int file_handler_2;

	char BufOut[4] = "Test";
	char BufIn[4] = "";

	file_handler_1 = open("/dev/etsele_cdev_0", O_WRONLY);
	file_handler_2 = open("/dev/etsele_cdev_1", O_RDONLY);

	if(file_handler_1 < 0 || file_handler_2 < 0)
	{
		printf("Erreur d'ouverture : %d, %d\n", file_handler_1, file_handler_2);
		return -1;
	}

	write(file_handler_1, &BufOut, 4);

	int nb_char = read(file_handler_2, &BufIn, 4);

	close(file_handler_1);
	close(file_handler_2);

	//printf("Buffer: %s\n", BufIn);

	if(BufIn[0] != 'T' && BufIn[0] != 'e' && BufIn[0] != 's' && BufIn[0] != 't')
	{
		return -1;
	}

	return 0;
}

/* With the serial port connected with its each other, try to write a command and read the same command.
 * in non-block mode.*/
int test_non_block_mode()
{
	int err;
	int file_handler_1 = -1;
	int file_handler_2 = -1;

	char BufOut[4] = "Test";
	char BufIn[4] = "";

	file_handler_1 = open("/dev/etsele_cdev_0", O_WRONLY | O_NONBLOCK);
	file_handler_2 = open("/dev/etsele_cdev_1", O_RDONLY | O_NONBLOCK);

	if(file_handler_1 < 0 || file_handler_2 < 0)
	{
		printf("Erreur d'ouverture : %d, %d\n", file_handler_1, file_handler_2);
		return -1;
	}

	write(file_handler_1, &BufOut, 4);

	int timeout = 0;
	int nb_char = 0;
	int nb_char_lu = 0;

	while(timeout < 10 && nb_char < 4)
	{
		nb_char_lu = read(file_handler_2, &BufIn, 4 - nb_char);

		printf("Nb char lut: %d\n", nb_char_lu);

		if(nb_char_lu > 0)
		{
			nb_char += nb_char_lu;
		}

		sleep(1);

		timeout++;
	}

	printf("Buffer: %s\n", BufIn);

	printf("%c\n", BufIn[0]);
	printf("%c\n", BufIn[1]);
	printf("%c\n", BufIn[2]);
	printf("%c\n", BufIn[3]);

	if(BufIn[0] != 'T' && BufIn[0] != 'e' && BufIn[0] != 's' && BufIn[0] != 't')
	{
		return -1;
	}

	close(file_handler_1);
	close(file_handler_2);

	return 0;
}

int test_ioctl()
{
	int fd;
	int err;

	int baudrate = 1000;

	fd = open("/dev/etsele_cdev_0", O_RDONLY | O_NONBLOCK);

	err = ioctl(fd, SERIAL_IOC_SET_BAUDRATE, (void*) baudrate);

	if(err < 0)
	{
		printf("LOL");
		return -1;
	}

	close(fd);

	return 0;
}




