#serial_driver-objs := buffer.o drv.o ioctl.o
#obj-m := serial_driver.o

obj-m += driver.o


all:
	make -C /usr/src/linux-headers-4.15.18+ M=`pwd` modules

#all:
#	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /usr/src/linux-headers-4.15.18+ M=$`pwd` clean

#clean:
#	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
